package com.saucelabs.sys_svc_access.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import com.android.internal.app.IBatteryStats;
import com.saucelabs.sys_svc_access.wrappers.ServiceManagerWrapper;
import com.saucelabs.sys_svc_access.wrappers.base.IServiceManagerWrapper;
import com.saucelabs.sys_svc_access.wrappers.exceptions.WrappedAccessException;

import java.util.Arrays;

import timber.log.Timber;

/**
 * A Service that allows interacting with SystemManager from command line. Commands can be sent
 * to this service by setting intent action to one of supported actions. All output is logged
 * and can be observed in logcat.
 *
 * List services:
 $ adb shell am startservice -a com.saucelabs.sys_svc_access.action.list \
       com.saucelabs.sys_svc_access/.services.SysSvcAccessService
> 09-29 04:20:38.008  2612  2612 I SysSvcAccessService: [sip, Genyd, SystemPatcher, ...

 * Get battery charging status:
 $ adb shell am startservice -a com.saucelabs.sys_svc_access.action.battery \
       com.saucelabs.sys_svc_access/.services.SysSvcAccessService
>  09-29 04:19:50.990  2612  2612 I SysSvcAccessService: Charging status: false

 */
public class SysSvcAccessService extends Service {

    public static final String ACTION_LIST_SVC = "com.saucelabs.sys_svc_access.action.list";
    public static final String ACTION_LIST_BATTERY = "com.saucelabs.sys_svc_access.action.battery";
    private IServiceManagerWrapper serviceManagerWrapper;

    /**
     * onCreate will try to instantiate a ServiceManagerWrapper which incoming commands will
     * use. The instantiation might throw a ClassNotFoundException in which case
     * serviceManagerWrapper will be null.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("Creating ServiceManagerWrapper");
        try {
            serviceManagerWrapper = new ServiceManagerWrapper();
            Timber.d("ServiceManagerWrapper created");
        } catch (ClassNotFoundException ex) {
            Timber.e(ex);
            Timber.e("Failed to initialize ServiceManagerWrapper");
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    /**
     *  Retrieves the intent action, handles the request and stops. Will abort right away in case
     *  serviceManagerWrapper is not available (null due to failed instantiation)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (serviceManagerWrapper == null) {
            Timber.e("No ServiceManagerWrapper available, stopping");
        } else if (intent != null){
            String action = intent.getAction();
            if (action == null) {
                Timber.e("No action given");
            } else if (ACTION_LIST_SVC.equals(action)) {
                handleListServicesCommand();
            } else if (ACTION_LIST_BATTERY.equals(action)) {
                handleGetChargeCommand();
            } else {
                Timber.e("Action %s not support", action);
            }
        }

        stopSelf(startId);
        return START_STICKY;
    }
    /**
     * Fetches running services list for {@link serviceManagerWrapper} and dumps them in logs.
     */
    private void handleListServicesCommand() {
        try {
            String[] services = serviceManagerWrapper.listServices();
            Timber.i(Arrays.toString(services));
        } catch (WrappedAccessException ex) {
            Timber.e("listServices failed");
        }
    }
    /**
     *  Fetches batterystats service from {@link serviceManagerWrapper} and logs the charging
     *  status.
     */
    private void handleGetChargeCommand() {
        try {
            IBinder binder = serviceManagerWrapper.getService(
                    ServiceManagerWrapper.SERVICE_BATTERYSTATS
            );
            IBatteryStats batteryStats = IBatteryStats.Stub.asInterface(binder);
            Timber.i("Charging status: %s", batteryStats.isCharging());
        } catch (WrappedAccessException | RemoteException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("Finished");
    }
}
