package com.saucelabs.sys_svc_access.wrappers.base;


import android.os.IBinder;

import com.saucelabs.sys_svc_access.wrappers.exceptions.WrappedAccessException;

public interface IServiceManagerWrapper {
    String[] listServices() throws WrappedAccessException;
    IBinder getService(String name) throws WrappedAccessException;
}
