package com.saucelabs.sys_svc_access.wrappers.exceptions;

public class WrappedAccessException extends Exception {
    public WrappedAccessException(Throwable e) {
       super(e);
    }
}
