package com.saucelabs.sys_svc_access.wrappers;

import android.annotation.SuppressLint;
import android.os.IBinder;


import com.saucelabs.sys_svc_access.wrappers.base.IServiceManagerWrapper;
import com.saucelabs.sys_svc_access.wrappers.exceptions.WrappedAccessException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import timber.log.Timber;

/**
 * This class serves as a wrapper to use for interacting with ServiceManager via reflection.
 * Methods names and signatures should mirror those in the target class whenever possible.
 * Static methods are presented as instance methods for a less verbose invocations
 * in terms of passed arguments.
 *
 * Any failed method calls should throw {@link WrappedAccessException}.
 */
public class ServiceManagerWrapper implements IServiceManagerWrapper {
    public static final String CLASS_SYSTEM_MANAGER = "android.os.ServiceManager";
    /**
     * Not exactly belonging to or used in this class, but this class is more relevant
     * than any other we have for holding names of services.
     */
    public static final String SERVICE_BATTERYSTATS = "batterystats";
    /**
     * Define names of different methods that will be accessed via reflection
     */
    private static final String METHOD_LIST_SERVICES = "listServices";
    private static final String METHOD_GET_SERVICE = "getService";
    /**
     * Class object associated with the wrapped ServiceManager
     */
    private Class<?> wrappedServiceManagerClass;

    @SuppressLint("PrivateApi")
    public ServiceManagerWrapper() throws ClassNotFoundException {
        this(Class.forName(CLASS_SYSTEM_MANAGER));
    }

    public ServiceManagerWrapper(Class wrappedServiceManagerClass) {
        this.wrappedServiceManagerClass = wrappedServiceManagerClass;
    }

    /**
     * Invokes the static method listServices against the wrapped SystemManager and returns the
     * result
     * @return List of all currently running services
     * @throws WrappedAccessException
     */
    @Override
    public String[] listServices() throws WrappedAccessException {
        Timber.d("listServices()");
        try {
            Timber.d("Trying to get method %s", METHOD_LIST_SERVICES);
            Method listServicesMethod = wrappedServiceManagerClass.getMethod(METHOD_LIST_SERVICES);

            Timber.d("Got method %s, invoking", listServicesMethod.getName());
            String[] result = (String[])listServicesMethod.invoke(wrappedServiceManagerClass);

            Timber.d("Invocation succeeded, returning %d services", result.length);
            return result;
        } catch (NoSuchMethodException e) {
            throw new WrappedAccessException(e);
        } catch (IllegalAccessException e) {
            throw new WrappedAccessException(e);
        } catch (InvocationTargetException e) {
            throw new WrappedAccessException(e);
        } catch (ClassCastException e) {
            throw new WrappedAccessException(e);
        }
    }

    /**
     * Invokes the static method getService(String) against the wrapped SystemManager and returns
     * the result
     * @param name Name of service to get
     * @return a reference to the service, or null if the service doesn't exist
     * @throws WrappedAccessException
     */
    @Override
    public IBinder getService(String name) throws WrappedAccessException {
        Timber.d("getService(%s)", name);

        try {
            Timber.d("Trying to get method %s", METHOD_GET_SERVICE);
            Method getServiceMethod = wrappedServiceManagerClass.getMethod(
                    METHOD_GET_SERVICE, String.class
            );

            Timber.d("Got method %s, invoking", getServiceMethod.getName());
            IBinder result = (IBinder) getServiceMethod.invoke(wrappedServiceManagerClass, name);

            Timber.d("Invocation succeeded, returning IBinder");
            return result;
        } catch (NoSuchMethodException e) {
            throw new WrappedAccessException(e);
        } catch (IllegalAccessException e) {
            throw new WrappedAccessException(e);
        } catch (InvocationTargetException e) {
            throw new WrappedAccessException(e);
        } catch (ClassCastException e) {
            throw new WrappedAccessException(e);
        }
    }
}
