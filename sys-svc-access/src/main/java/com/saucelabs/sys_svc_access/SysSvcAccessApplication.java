package com.saucelabs.sys_svc_access;

import android.app.Application;

import timber.log.Timber;

public class SysSvcAccessApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
