package com.android.internal.app;

interface IBatteryStats {
    boolean isCharging();
}
