## SysSvcAccessApplication

- Application entry point. The only reason this is there is to configure logging.
- Logging is done through Timber
- Log level is Debug and with no way to change it without updating it in code. Not ideal but
okay for the scope of the task.

## SysSvcAccessService

A Service that allows interacting with SystemManager from command line. Commands can be sent
to this service by setting intent action to one of supported actions. All output is logged
and can be observed in logcat.

List services:
```
$ adb shell am startservice -a com.saucelabs.sys_svc_access.action.list \
com.saucelabs.sys_svc_access/.services.SysSvcAccessService
> 09-29 04:20:38.008  2612  2612 I SysSvcAccessService: [sip, Genyd, SystemPatcher, ... 
```

Get battery charging status:
```
$ adb shell am startservice -a com.saucelabs.sys_svc_access.action.battery \
com.saucelabs.sys_svc_access/.services.SysSvcAccessService
>  09-29 04:19:50.990  2612  2612 I SysSvcAccessService: Charging status: false
```

## IServiceManagerWrapper

An IServiceManagerWrapper describes methods available in ServiceManager that are going to be
used. Methods names and signatures should mirror those in ServiceManager whenever possible as
this will be good for maintainability on the long run. Having an interface will also facilitate
testing, allow swapping the implementation if ever necessary.

The class `ServiceManagerWrapper` implements ` IServiceManagerWrapper` by using reflection against
ServiceManager to fulfill the method invocations.

## IBatteryStats.aidl

In order to have access to IBatteryStats and its methods at compile time, the dummy aidl interface
`com.android.internal.app.IBatteryStats.aidl` was necessary to have in project files.
This was enough to generate the associated java class for a successful compilation. During runtime,
access to the class will resolve to the real one.